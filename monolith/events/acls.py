import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 1,
    }
    url = "https://api.pexels.com/v1/search"
    res = requests.get(url, params=params, headers=headers)
    content = json.loads(res.content)

    photo = {}
    try:
        photo["picture_url"] = content["photos"][0]["src"]["original"]
    except (KeyError, IndexError):
        photo["picture_url"] = None
    # return photo
    return photo

    # return {"picture_url": content["photos"][0]["src"]["original"]}
    # print(res.content)


def get_weather_data(city):
    # get lat, lon for the conference location
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    content = json.loads(res.content)

    coord = {}
    try:
        coord["lat"] = content[0]["lat"]
        coord["lon"] = content[0]["lon"]
    except (KeyError, IndexError):
        coord["lat"] = None
        coord["lon"] = None

    lat = coord["lat"]
    lon = coord["lon"]
    # get weather data
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    res = requests.get(url)
    content = json.loads(res.content)

    weather = {}
    try:
        weather["temp"] = content["main"]["temp"]
        weather["weather_description"] = content["weather"][0]["description"]
    except (KeyError, IndexError):
        weather["temp"] = None
        weather["weather_description"] = None
    return weather
