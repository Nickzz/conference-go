import json
import requests

from .models import ConferenceVO


def get_conference():
    url = "http://monolith:8000/api/conferences/"
    # send a GET reqeust to /api/conferencees
    response = requests.get(url)
    content = json.loads(response.content)
    # for each conference in conferences:
    for conference in content.conference:
        #####create a brand new conferenceVO if it does not exist, otherwise update it
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"]
            defaults={
                "name": conference["name"]
            }
        )
